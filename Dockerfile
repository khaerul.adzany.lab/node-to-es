FROM telkomindonesia/alpine:nodejs-10

ENV ES_HOST=http://10.1.95.50:9200


# Set Working Directory
WORKDIR /usr/src/app

# Copy Node Packages Requirement
COPY package*.json ./

# Install Node Modules Based On Node Packages Requirement
RUN apk update \
      build-base \
      git \
      python \
      python-dev \
    && npm i -g npm \
    && npm i -g node-gyp \
    && npm install --legacy-peer-deps

# Copy Node Source Code File
COPY . .

# Expose Application Port
EXPOSE 3000

# Run The Application
CMD ["npm", "start"]