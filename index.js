const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json())
const es = require('./es')

app.get('/create/:indexName', (req, res) => {
    let indexName = req.params['indexName']
    es.createIndex(indexName)
    res.send(req.params)
})

app.post('/add/:indexName', (req, res, next) => {
    let indexName = req.params['indexName']
    let body = req.body
    es.initMapping(indexName)
    es.addDocument(indexName, body)
    res.sendStatus(200)
})

app.listen(3000, () => {
    es.healthCheck()
})