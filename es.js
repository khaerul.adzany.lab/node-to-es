var es = require('elasticsearch')

const esClient = new es.Client({
    host: 'http://10.1.95.50:9200',
    httpAuth: 'elastic:umeetme',
    log: 'info'
})

const healthCheck = () => {
    esClient.cluster.health({}, (err, res) => {
        if(!err){
            console.log(res)
        }
    })
}

exports.healthCheck = healthCheck

const indexExists = (idxName) => {
    return esClient.indices.exists({
        index: idxName
    })
}

const createIndex = (idxName) => {
    indexExists(idxName).then((e) => {
        if(e) return

        return esClient.indices.create({
            index: idxName
        }, (err, res, status) => {
            if(err){
                console.error(err)
                return
            }
            console.log('created index: ', res, status)
        })
    });
}

exports.createIndex = createIndex

const mapExists = (idxName) => {
    return esClient.indices.getMapping({
    index: idxName
    })
}

const initMapping = (idxName) => {
    indexExists(idxName).then((e) => {
        if(!e) return
        mapExists(idxName).then((e) => {
            if(e[idxName].mappings['@timestamp'] == undefined){
                return esClient.indices.putMapping({
                    index: idxName,
                    type: 'document',
                    includeTypeName: true,
                    body: {
                        "properties": {
                            "@timestamp": {
                              "type": "date"
                            },
                            "call_duration": {
                              "type": "double"
                            },
                            "client": {
                              "type": "keyword"
                            },
                            "is_recording": {
                              "type": "keyword"
                            },
                            "participant": {
                              "type": "long"
                            },
                            "rec_duration": {
                              "type": "double"
                            },
                            "recording_url": {
                              "type": "keyword"
                            },
                            "room": {
                              "type": "keyword"
                            }
                        }
                    }
                })
            }
        })
    })
}

exports.initMapping = initMapping

const addDocument = (idxName, document) => {
    indexExists(idxName).then((e) => {
        if(!e) return
        return esClient.index({
            index: idxName,
            type: 'document',
            body: {
                "@timestamp": document.timestamp,
                "call_duration": document.call_duration,
                "client": document.client,
                "is_recording": document.is_recording,
                "participant": document.participant,
                "rec_duration": document.rec_duration,
                "recording_url": document.recording_url,
                "room": document.room
            }
        }, (err, res) => {
            if(!err) console.error(res)
        })
    })
}

exports.addDocument = addDocument